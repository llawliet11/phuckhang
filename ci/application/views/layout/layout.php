<?php
get_header();
?>

<?php if (isset($content)) : ?>

	<?php
	/**
	 * This page has been modified
	 * The git for this repo is available. please check carefully if you want to rollback this file
	 * @author Nghia.Pham <pham.d.nghia@gmail.com>
	 * $leftContentHtmlClasses : see partial/left_content.php
	 * $mainContentHtmlClasses, $rightContentHtmlClasses are set with your default classes
	 */


	$mainContentHtmlClasses = 'col-lg-6 col-lg-offset-0 col-md-7 col-md-offset-0 col-sm-7 col-sm-offset-0 col-xs-10 col-xs-offset-1';
	$rightContentHtmlClasses = 'col-lg-4';

	if (is_page_template('page-landbook.php')) {
		$mainContentHtmlClasses = 'col-xs-12 col-sm-12 col-md-9 col-lg-9';
		$rightContentHtmlClasses .= ' hidden';
	}

	?>


	<div id="user-profile" class="content social-cotent">

		<div class="row">


			<?php if (isset($content['left'])): ?>
				<?php echo $content['left']; ?>
			<?php endif; ?>

			<div class="<?php echo $mainContentHtmlClasses; ?>">
				<div id="user_status_action" class="bg bg-success">

					<!--Desktop Size-->
					<ul id="" class="clearfix ">

						<li class="col-sm-4 col-xs-12 text-left">
							<a href="#" class="fa-pkn-human">Hồ sơ của tôi</a>
						</li>

						<li class="col-sm-4 col-xs-12  text-center ">
							<a href="#" class="fa-pkn-message">
								Nhắn tin
								<span class="fa-stack">
							      <i class="fa fa-circle fa-stack-2x  text-gray"></i>
							      <i class="fa fa-inverse fa-stack-1x fs12"><b>12</b></i>
						        </span>
							</a>

						</li>

						<li class="col-sm-4  col-xs-12  text-right">
							<a href="#" class="fa-pkn-sound">
								Thông báo
								<span class="fa-stack">
							      <i class="fa fa-circle fa-stack-2x text-gray"></i>
							      <i class="fa fa-inverse fa-stack-1x fs12"><b>12</b></i>
						        </span>
							</a>

						</li>
					</ul>
					<!--Desktop Size-->
				</div>

				<?php if (isset($content['main'])): ?>
					<?php echo $content['main']; ?>
				<?php endif; ?>
			</div>

			<div class="<?php echo $rightContentHtmlClasses; ?>">
				<?php if (isset($content['right'])): ?>
					<?php echo $content['right']; ?>
				<?php endif; ?>
			</div>

		</div>
	</div>

	<div class="clearfix"></div>

	<div class="horizontal-footer"></div>
<?php endif ?>
<?php
get_footer();
?>
